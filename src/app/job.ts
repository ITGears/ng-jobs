export class Job {
    id: number;
    title: string;
    thumbnail: string;
    attachments: string[];
    description: string;
}
