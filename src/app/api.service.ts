import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from "@angular/common/http";

import { Job } from './job';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
    
  apiJobsUrl: string = 'http://localhost:3030/jobs';

  constructor(private httpClient: HttpClient) { }
  
  get_jobs(): Observable<Job[]> {
      return this.httpClient.get<Job[]>(this.apiJobsUrl + '/?state=active');
  }
  
  get_job(id: number): Observable<Job> {
      return this.httpClient.get<Job>(this.apiJobsUrl + `/?id=${id}`);
  }
}
