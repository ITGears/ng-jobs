import { Component, OnInit } from '@angular/core';
import { Job } from '../job';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.sass']
})
export class JobsComponent implements OnInit {

  jobs: Job[] = [];
  jobsLoading: boolean = false;

  constructor(
    private http: HttpClient,
    private apiService: ApiService
  ) { }

  ngOnInit() {
    this.jobsLoading = true;
    this.get_jobs();
  }
  
  public get_jobs() {
    this.apiService.get_jobs().subscribe((data: Job[]) => {
      this.jobs = data;
      this.jobsLoading = false;
    });
  }

}
