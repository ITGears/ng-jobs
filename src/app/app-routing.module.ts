import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { JobDetailsComponent } from './job-details/job-details.component';
import { NotFoundComponent } from './not-found/not-found.component';

const routes: Routes = [
  {
     path: "job/:id",
     component: JobDetailsComponent
    },
   {
     path: "**",
     component: NotFoundComponent
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
