import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from "@angular/common/http/testing";
import { HttpClient } from '@angular/common/http';

import { ApiService } from './api.service';
import { Job } from './job';

describe('ApiService', () => {
    
  let httpClient: HttpClient;
  let httpMock: HttpTestingController;
  let service: ApiService;
    
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      providers: [ ApiService ]
    });
    
    httpClient = TestBed.get(HttpClient);
    httpMock = TestBed.get(HttpTestingController);
    service = TestBed.get(ApiService);
  });
  
  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    const service: ApiService = TestBed.get(ApiService);
    expect(service).toBeTruthy();
  });
  
  describe('## Jobs', () => {
    
    let dummyJobs: Job[];
    
    beforeEach(() => {
      service = TestBed.get(ApiService);
      dummyJobs = [
        { id: 1, title: 'title1', thumbnail: '', attachments: [], description: '' },
        { id: 2, title: 'title2', thumbnail: '', attachments: [], description: '' }
      ] as Job[];
    });
    
    it('should return expected number of jobs (called ONCE) via GET', () => {
      service.get_jobs().subscribe((jobs) => {
        expect(jobs.length).toBe(dummyJobs.length);
      });
      const req = httpMock.expectOne(service.apiJobsUrl + '/?state=active');
      expect(req.request.method).toEqual('GET');
      req.flush(dummyJobs);
    });
    
    it('should return expected number of jobs (called MULTIPLE times)', () => {
      service.get_jobs().subscribe();
      service.get_jobs().subscribe();
      service.get_jobs().subscribe(jobs => expect(jobs).toEqual(dummyJobs, 'should return expected jobs'));
      const requests = httpMock.match(service.apiJobsUrl + '/?state=active');
      expect(requests.length).toEqual(3, 'calls of Jobs()');
      requests[0].flush([]);
      requests[1].flush([{ id: 2, name: 'title2', thumbnail: '', attachments: [], description: '' }]);
      requests[2].flush(dummyJobs);
    });
    
    it('should OK to return NO jobs', () => {
      service.get_jobs().subscribe(jobs => expect(jobs.length).toBe(0, "should have no jobs"));
      const req = httpMock.expectOne(service.apiJobsUrl + "/?state=active");
      req.flush([]);
    });
    
    it('should get single hero details', () => {
      const singleJob = {id: 33, title: "title2", thumbnail: "", attachments: [], description: ""};
      service.get_job(33).subscribe(
        job => expect(job).toEqual(singleJob, 'should get hero details')
      );
      const req = httpMock.expectOne(service.apiJobsUrl + "/?id=33");
      expect(req.request.method).toEqual("GET");
      req.flush(singleJob);
    });
      
  });
  
});
