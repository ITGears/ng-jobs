# Karma Jobs Tests
> Tests are good documentation.


[![1.0.0][travis-image]][travis-url]

A tiny demonstration of how to use for Karma tests.


## Installation

Run JSON server:

```sh
cd node-server
bash run.sh
```
Start the Angular application:

```
ng serve -o
```

Run Karma tests:

```
ng test
```

Enjoy adding new tests.. ;-)

## Author

Sergey Markov – sergey.markov@gmail.com

[https://bitbucket.org/ITGears/ng-jobs](https://bitbucket.org/ITGears/ng-jobs)


<!-- Markdown link & img dfn's -->
[travis-image]: https://img.shields.io/travis/dbader/node-datadog-metrics/master.svg?style=flat-square
[travis-url]: https://travis-ci.org/dbader/node-datadog-metrics
[wiki]: https://github.com/yourname/yourproject/wiki
